<?php
class UsuarioController extends RestController
{
    public function get($id)
    {
        $this->data = ((new Usuario)->find_first($id));

    }
    public function getAll()
    {
        $this->data = ((new Usuario)->find());
    }
    public function post() {
        if ($this->param()) {
            $data = $this->param();
            $this->data = (New Usuario)->create($data);
        }
    }
}