<?php
class PedidosController extends RestController
{
    public function get($id)
    {
        $this->data = ((new Pedidos)->find_first($id));

    }
    public function getAll()
    {
        $this->data = ((new Pedidos)->find());
    }
    public function post() {
        if ($this->param()) {
            $data = $this->param();
            $this->data = (New Pedidos)->create($data);
        }
    }
}