<?php
class PersonaController extends RestController
{
    public function get($id)
    {
        $this->data = ((new Persona)->find_first($id));

    }
    public function getAll()
    {
        $this->data = ((new Persona)->find());
    }
    public function post() {
        if ($this->param()) {
            $data = $this->param();
            $this->data = (New Persona)->create($data);
        }
    }
}