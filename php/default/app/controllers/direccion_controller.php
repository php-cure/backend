<?php
class DireccionController extends RestController
{
    public function get($id)
    {
        $this->data = ((new Direccion)->find_first($id));

    }
    public function getAll()
    {
        $this->data = ((new Direccion)->find());
    }
    public function post() {
        if ($this->param()) {
            $data = $this->param();
            $this->data = (New Direccion)->create($data);
        }
    }
}